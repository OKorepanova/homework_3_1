'use strict'

var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log ('Задание 1');

function Student(name, points){
  this.name = name;
  this.points = points;
}

Student.prototype.show=function(){
    console.log('Студент %s набрал %d баллов', this.name, this.points);
  }

console.log ('Задание 2');

function StudentList(group, studentsPoints){
  this.title = group;
  if (studentsPoints) {
      for (var i = 0; i < studentsPoints.length; i += 2){
        this.add(studentsPoints[i], studentsPoints[i+1]);
      };
    }
}

StudentList.prototype=Object.create(Array.prototype);

// добавление нового студента в список по имени и количеству баллов
StudentList.prototype.add = function (name, points){
  if (typeof name === 'string' && typeof points === 'number'){
    this.push(new Student(name, points)); 
  } 
  else {
    console.log ('Данные некорректны');
  }
}

console.log ('Задание 3');

var hj2 = {};
hj2 = new StudentList('HJ-2', studentsAndPoints);
console.log(hj2);

console.log ('Задание 4');

hj2.add ('Иванов Иван', 50); 
hj2.add ('Петров Петр', 100); 
hj2.add ('Сидоров Сидр', 10); 
console.log(hj2);

console.log ('Задание 5');

var html7 = [];
var stAndPnt = [];
html7 = new StudentList('HTML-7');
console.log(html7);
html7.add ('Васечкин Вася', 40);
html7.add ('Тупицын Федор', 0);
html7.add ('Умная Маша', 200);
console.log(html7);

console.log ('Задание 6');

// вывод списка группы
StudentList.prototype.show = function(){
  console.log('Группа %s (%d студентов):', this.title, this.length);
  this.forEach(function (value,i){
     value.show();
  }, this);
}
html7.show();
hj2.show();

console.log ('Задание 7');

// перевод студента из группы в группу
StudentList.prototype.transfer = function (name, group2){
  var transferIndex;
  transferIndex=this.findIndex(function(value){
    return (value.name == name);
  });
  group2.push(this[transferIndex]);
  this.splice(transferIndex, 1);
  return;
}

html7.transfer('Тупицын Федор',hj2);
html7.show();
hj2.show();

console.log ('Задание 8');

//Переопределим valueOf, так чтобы он возвращал баллы объекта Student
Student.prototype.valueOf = function(){
  return this.points;
}

StudentList.prototype.max = function(){  
  var maxPoints = Math.max.apply(null, this); //находим максимальный балл, пользуясь тем, что Math.max вызывает valueOf элемента
  var maxName = this.find(function(value){  // ищем студента по найденному максимальному баллу
    return value.points >= maxPoints;
  });
  return maxName.name; // возвращаем имя студента
}

console.log(html7.max());

