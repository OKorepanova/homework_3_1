var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log ('Задание 1');

var students=[];
for (i = 0; i < studentsAndPoints.length; i += 2){
  addStudent(studentsAndPoints[i], studentsAndPoints[i+1]);
}

students.forEach(function(value, i){
  value.show();
})

console.log ('');
console.log ('Задание 2');

function addStudent (addName, addPoints){
  students.push ({name: addName, points: addPoints, 
  show: function () {
    console.log('Студент ' + this.name + ' набрал ' + this.points + ' баллов')
    }
  })
}

addStudent ('Николай Фролов', 0);
addStudent ('Олег Боровой', 0);


students.forEach(function(value, i){
  value.show();
})

console.log ('');
console.log ('Задание 3');

students.forEach(function(value, i){
  if (students[i].name == 'Ирина Овчинникова' || students[i].name == 'Александр Малов'){
    students[i].points += 30;
  }
  if (students[i].name == 'Николай Фролов'){
    students[i].points += 10;
  }
})

students.forEach(function(value, i){
  value.show();
})

console.log ('');
console.log ('Задание 4');
console.log ('Список студентов:');

students.forEach(function(value){
  if (value.points >= 30){
    value.show();
  }
})

console.log ('');
console.log ('Задание 5');

students.forEach(function(value){  //каждому студенту
  value.worksAmount = (value.points/10); // добавляем свойство worksAmount
})

console.log ('');
console.log ('Задание 6');

students.findByName = function  (value){
  var student=this.find(function(valueFind){
    return (valueFind.name == value);
  });
  return student;
}

students.findByName('Ирина Овчинникова').show();



